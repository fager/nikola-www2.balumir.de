.. title: Gitlab CI/CD für das Deployment statischer Webseite - Ein Anfang
.. slug: gitlab-cicd-fur-das-deployment-statischer-webseite-ein-anfang
.. date: 2020-10-08 23:26:06 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Gitlab und dabei auch Gitlab CI/CD beschäftigt und fasziniert mich bereits
seit einer geraumen Zeit. Auf dem LUG Camp 2019 hat es mit einem längeren
Git-Workshop angefangen und nun wird es zeit auch das Deployment über
Gitlab CI/CD erledigen zu lassen.

Diese Webseite pflege ich mit Nikola_. Über eine CI/CD Wird zuerst mit 
einem Python Image die Webseite erstellt und danach mit
meinem Deployment-Helper_ Image per sftp auf meinen Webserver geschoben.

Die Zugangsdaten dazu sind als Umgebungsvariablen im Gitlab hinterlegt
und werden von Gitlab automatisch maskiert, damit diese nicht in den Logs
auftauchen.

Zusätzlich ist der ssh-Dienst auf meiner Webseite so konfiguriert, dass
über den Account lediglich ein sftp-Upload in ein chroot-Verzeichnis
erfolgen kann. Selbst wenn also jemand an die Daten kommt, könnte er
höchstens statische Webseite austauschen. Mehr geht aber nicht.

Ausführlicher kommt das ganze dann in den nächsten Artikeln...

.. _Nikola: https://getnikola.com/
.. _Deployment-Helper: https://gitlab.com/fager/deployment-helper

